#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# Copyright (C) 2022  Blau Araujo <blau@debxp.org>
# License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
# ------------------------------------------------------------------------------

version=0.0.1

prefix='Uptime: '

format='%A, %d de %B de %Y, às %Rh'

u[0]='dia'
u[1]='hora'
u[2]='minuto'

help="upt ($version) - Tempo de atividade do sistema.

USO
    upt [OPÇÕES]

DESCRIÇÃO
    Exibe o tempo de atividade do sistema.

OPÇÕES
    -d  Exibe seperação com vírgulas.
    -p  Exibe prefixo antes do tempo de atividade.
    -s  Data e hora do início da atividade.
    -h  Exibe esta ajuda e sai.

Copyright (C) 2022  Blau Araujo <blau@debxp.org>

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
"

seconds() {
    : "$(< /proc/uptime)"
    s=${_/.*}
}

usage() {
    echo "$help"
    exit $error
}

since() {
    local day
    day=$(date -d"@$(( $(date +%s) - $s))" "+$format")
    echo "${day^}"
    exit
}

while getopts ':hspd' opt; do
    case $opt in
        h) usage         ;;
        p) uptime=$prefix;;
        d) sep=','       ;;
	s) seconds; since;;
        *) error=1; usage;;
    esac
done

seconds

t[0]=$((s / 60 / 60 / 24)) # Days
t[1]=$((s / 60 / 60 % 24)) # Hours
t[2]=$((s / 60 % 60))      # Minutes

for i in "${!t[@]}"; do
    (( ${t[i]} )) && uptime+="${t[i]} ${u[i]}" || continue
    (( ${t[i]} > 1 )) && uptime+="s"
    uptime+="$sep "
done

[[ "${uptime: -2}" = "$sep " ]] && uptime=${uptime::-2}

echo $uptime

exit
