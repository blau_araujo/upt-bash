# Uptime em Bash

Informação do tempo de atividade do sistema.

[![asciicast](https://asciinema.org/a/2I6Lw4LawmKaEHw1JA5ySlMHX.svg)](https://asciinema.org/a/2I6Lw4LawmKaEHw1JA5ySlMHX)

## Dependências

- GNU date (GNU coreutils)

## Uso:

```
SINTAXE
    upt [OPÇÕES]

OPÇÕES
    -d  Exibe seperação com vírgulas.
    -p  Exibe prefixo antes do tempo de atividade.
    -s  Data e hora do início da atividade.
    -h  Exibe esta ajuda e sai.
```

